package com.lockscreenperks.api

import com.lockscreenperks.prefs.AppPrefs
import com.lockscreenperks.screens.Const
import com.lockscreenperks.screens.home.model.LoginResponse
import com.lockscreenperks.screens.lockscreen.search.model.Search
import com.lockscreenperks.screens.lockscreen.suggest.model.Suggestion
import com.lockscreenperks.utils.Utils
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AppApi {

    private val loginService: AppService.LoginService
    private val suggestService: AppService.SuggestService
    private val searchSearch: AppService.SearchService

    init {
        loginService = Retrofit.Builder()
            .baseUrl(Const.LOGIN_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(AppService.LoginService::class.java)

        suggestService = Retrofit.Builder()
            .baseUrl(Const.SUGGEST_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(AppService.SuggestService::class.java)

        searchSearch = Retrofit.Builder()
            .baseUrl(Const.SEARCH_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(AppService.SearchService::class.java)
    }

    fun login(adId: String, apiKey: String): Call<LoginResponse> {
        return loginService.login(adId, apiKey)
    }

    fun getSuggestion(key: String): Call<Suggestion> {
        return suggestService.getSuggestion(
            Const.PARTNER_SUGGEST_NAME,
            key,
            Const.SUGGEST_SUB1,
            Const.SUGGEST_VERSION,
            Utils.getUserAgent(),
            Utils.getDefaultRfr(),
            Utils.getDeviceId(),
            Const.DEFAULT_MOBILE_TYPE
        )
    }

    fun search(key:String): Call<Search> {
        return searchSearch.search(
            partner = Const.PARTNER_SEARCH_NAME,
            qt = key,
            ip = Utils.getIPAddress(true),
            ua = Utils.getUserAgent(),
            rfr = Utils.getDefaultRfr(),
            mIdfa = AppPrefs.get().gpsAdId,
            v = Const.SEARCH_VERSION,
            results = 2,
            sub1 = Const.SEARCH_SUB1,
            out = Const.TYPE_JSON,
            web = 1,
            webResults = Const.DEFAULT_NUM_WEB_RESULT
        )
    }


}