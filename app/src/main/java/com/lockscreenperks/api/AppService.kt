package com.lockscreenperks.api

import com.lockscreenperks.screens.home.model.LoginResponse
import com.lockscreenperks.screens.lockscreen.search.model.Search
import com.lockscreenperks.screens.lockscreen.suggest.model.Suggestion
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface AppService {
    interface LoginService {
        @GET("/buzzvil/login")
        fun login(@Query("ad_id") adId: String, @Query("api_key") apiKey: String): Call<LoginResponse>
    }

    interface SuggestService {
        @GET("/suggestions")
        fun getSuggestion(
            @Query("partner") partner: String,
            @Query("qt") qt: String,
            @Query("sub1") sub1: String,
            @Query("v") v: String,
            @Query("ua") ua: String,
            @Query("rfr") rfr: String,
            @Query("mobile-id") mobileId: String,
            @Query("mobile-type") mobileType: Int
        ): Call<Suggestion>
    }

    interface SearchService {
        @GET("/xmlamp/feed")
        fun search(
            @Query("partner") partner: String,
            @Query("qt") qt: String,
            @Query("ip") ip: String,
            @Query("ua") ua: String,
            @Query("rfr") rfr: String,
            @Query("m-idfa") mIdfa: String,
            @Query("v") v: String,
            @Query("results") results: Int,
            @Query("sub1") sub1: String,
            @Query("out") out: String,
            @Query("web") web: Int,
            @Query("web-results") webResults: Int
        ): Call<Search>
    }
}