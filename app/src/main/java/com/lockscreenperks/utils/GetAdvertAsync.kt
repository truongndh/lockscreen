package com.lockscreenperks.utils

import android.os.AsyncTask
import android.text.TextUtils
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.lockscreenperks.App
import com.lockscreenperks.prefs.AppPrefs
import java.io.IOException

class GetAdvertAsync(private val onSuccess: () -> Unit) : AsyncTask<Void, Void, String>() {

    override fun doInBackground(vararg params: Void): String {
        var advertID = ""
        try {
            val idInfo = AdvertisingIdClient.getAdvertisingIdInfo(App.get())
            advertID = idInfo?.id ?: ""
        } catch (e: GooglePlayServicesNotAvailableException) {
            e.printStackTrace()
        } catch (e: GooglePlayServicesRepairableException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return advertID
    }

    override fun onPostExecute(advertID: String) {
        super.onPostExecute(advertID)

        if (!TextUtils.isEmpty(advertID)) {
            AppPrefs.get().gpsAdId = advertID
        }
        onSuccess()
    }

}