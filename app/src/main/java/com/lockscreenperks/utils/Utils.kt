package com.lockscreenperks.utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.provider.Settings
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import com.lockscreenperks.App
import com.lockscreenperks.screens.Const
import java.net.NetworkInterface.getNetworkInterfaces
import java.text.SimpleDateFormat
import java.util.*


object Utils {


    fun genRandomUUID(): String {
        return UUID.randomUUID().toString()
    }

    fun getUserAgent(): String {
        return System.getProperty("http.agent") ?: ""
    }

    fun getDeviceId(): String {
        return Settings.Secure.getString(App.get().contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun openDeepLink(context: Context?, link: String) {
        val openURL = Intent(Intent.ACTION_VIEW)
        openURL.data = Uri.parse(link)
        try {
            context?.startActivity(openURL)
        } catch (e: ActivityNotFoundException) {
        }
    }

    fun getCurrentYear(): Int {
        val cal = Calendar.getInstance(TimeZone.getDefault())
        return cal.get(Calendar.YEAR)
    }

    fun openBrowser(context: Context?, url: String) {
        val openURL = Intent(Intent.ACTION_VIEW)
        openURL.data = Uri.parse(url)
        context?.startActivity(openURL)
    }

    fun highlight(source: String, highlight: String): SpannableStringBuilder {
        var lcs = lcs(source.toLowerCase(), highlight.toLowerCase())
        val tLower = source.toLowerCase()
        val builder = SpannableStringBuilder()
        builder.append(source)
        var i = 0
        while (i < tLower.length && lcs.isNotEmpty()) {
            if (tLower[i] == lcs[0]) {
                builder.setSpan(StyleSpan(Typeface.BOLD), i, i + 1, 0)
                lcs = lcs.substring(1)
            }
            i++
        }
        return builder
    }

    private fun lcs(text1: String, text2: String): String {
        val lengths = Array(text1.length + 1) { IntArray(text2.length + 1) }
        for (i in 0 until text1.length) {
            for (j in 0 until text2.length) {
                if (text1[i] == text2[j]) {
                    lengths[i + 1][j + 1] = lengths[i][j] + 1
                } else {
                    lengths[i + 1][j + 1] = Math.max(lengths[i + 1][j], lengths[i][j + 1])
                }
            }
        }
        val sb = StringBuffer()
        var x = text1.length
        var y = text2.length
        while (x != 0 && y != 0) {
            when {
                lengths[x][y] == lengths[x - 1][y] -> x--
                lengths[x][y] == lengths[x][y - 1] -> y--
                else -> {
                    assert(text1[x - 1] == text2[y - 1])
                    sb.append(text1[x - 1])
                    x--
                    y--
                }
            }
        }
        return sb.reverse().toString()
    }

    fun getNextTime(time: Int): String {
        val simpleDateFormat = SimpleDateFormat(Const.TIME_FORMAT, Locale.getDefault())
        return simpleDateFormat.format(Date(System.currentTimeMillis() + time))
    }

    fun getIPAddress(useIPv4: Boolean): String {
        try {
            Collections.list(getNetworkInterfaces()).forEach { networkInterface ->
                Collections.list(networkInterface.inetAddresses).forEach { address ->
                    if (!address.isLoopbackAddress) {
                        val hostAddress = address.hostAddress
                        val isIPv4 = hostAddress.indexOf(':') < 0

                        if (useIPv4) {
                            if (isIPv4)
                                return hostAddress
                        } else {
                            if (!isIPv4) {
                                val delim = hostAddress.indexOf('%')
                                return if (delim < 0) hostAddress.toUpperCase() else hostAddress.substring(
                                    0,
                                    delim
                                ).toUpperCase()
                            }
                        }
                    }
                }
            }
        } catch (ignored: Exception) {
        }
        return ""
    }

    fun getDefaultRfr(): String {
        return ""
    }
}