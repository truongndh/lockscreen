package com.lockscreenperks.utils

import android.view.View
import android.widget.FrameLayout
import com.google.android.material.snackbar.Snackbar
import com.lockscreenperks.R

object SnackbarUtil {

    fun showSnackbar(
        rootView: View?,
        msg: CharSequence,
        action: String? = null,
        onAction: (() -> Unit)? = null,
        duration: Int = Snackbar.LENGTH_LONG
    ) {
        rootView?.let {
            val margin = it.resources.getDimensionPixelOffset(R.dimen.space_small)

            val snackbar = Snackbar.make(it, msg, duration).apply {
                view.setBackgroundResource(R.drawable.bg_snackbar)
                val params = view.layoutParams as FrameLayout.LayoutParams
                params.leftMargin = margin
                params.rightMargin = margin
                params.bottomMargin = margin
                view.layoutParams = params
            }
            action?.let {
                snackbar.setAction(R.string.undo) {
                    onAction?.invoke()
                }
            }
            snackbar.show()
        }

    }
}