package com.lockscreenperks.utils

import android.widget.ImageView

object ImageUtil {

    fun loadImage(uri: Any?, image: ImageView) {
        GlideApp.with(image.context)
            .load(uri)
            .into(image)
    }
}