package com.lockscreenperks.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.lockscreenperks.R

object DialogUtil {

    fun showSingleChoiceDialog(
        context: Context? = null,
        title: String? = null,
        items: Array<out CharSequence>,
        checkedItem: Int = 0,
        onPositive: ((Int) -> Unit)? = null,
        onNegative: (() -> Unit)? = null
    ) {
        context?.let {
            var selected: Int = 0
            AlertDialog.Builder(it)
                .setTitle(title)
                .setSingleChoiceItems(items, checkedItem) { _, which ->
                    selected = which
                }
                .setPositiveButton(R.string.ok) { _, _ -> onPositive?.invoke(selected) }
                .setNegativeButton(R.string.cancel) { _, _ -> onNegative?.invoke() }
                .show()
        }
    }

    fun showListDialog(
        context: Context? = null,
        title: String? = null,
        items: Array<out CharSequence>,
        onSelect: ((Int) -> Unit)? = null
    ) {
        context?.let {
            AlertDialog.Builder(it)
                .setTitle(title)
                .setItems(items) { _, which ->
                    onSelect?.invoke(which)
                }
                .show()
        }
    }

    fun showConfirmDialog(
        context: Context? = null,
        title: String? = null,
        message: String? = null,
        onPositive: (() -> Unit)? = null,
        onNegative: (() -> Unit)? = null
    ) {
        context?.let {
            AlertDialog.Builder(it)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok) { _, _ -> onPositive?.invoke() }
                .setNegativeButton(R.string.cancel) { _, _ -> onNegative?.invoke() }
                .show()
        }
    }
}