package com.lockscreenperks.screens.chooseapp.model

import android.os.Parcel
import android.os.Parcelable

data class SlugApp(val name: String?, val slug: String?) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(slug)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SlugApp> {
        override fun createFromParcel(parcel: Parcel): SlugApp {
            return SlugApp(parcel)
        }

        override fun newArray(size: Int): Array<SlugApp?> {
            return arrayOfNulls(size)
        }
    }
}