package com.lockscreenperks.screens.chooseapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.buzzvil.buzzscreen.sdk.BuzzScreen
import com.lockscreenperks.R
import com.lockscreenperks.extensions.changeRoot
import com.lockscreenperks.extensions.replace
import com.lockscreenperks.screens.chooseapp.model.SlugApp
import com.lockscreenperks.screens.main.MainFragment
import com.lockscreenperks.screens.profile.ProfileFragment
import com.lockscreenperks.utils.DialogUtil
import kotlinx.android.synthetic.main.fragment_choose_app.*

class ChooseAppFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_choose_app, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        spinnerChooseApp.setOnClickListener { _ ->
            arguments?.getParcelableArrayList<SlugApp>(KEY_DATA)?.let {
                showListAppDialog(it.toTypedArray())
            }
        }
    }

    private fun showListAppDialog(appList: Array<SlugApp>) {
        DialogUtil.showListDialog(context = context,
            title = getString(R.string.choose_list_app_title),
            items = appList.map { it.name ?: "" }.toTypedArray(),
            onSelect = { showDialogConfirmChooseApp(appList[it]) }
        )
    }

    private fun showDialogConfirmChooseApp(app: SlugApp) {
        DialogUtil.showConfirmDialog(context = context,
            message = getString(R.string.msg_confirm_connect_app, app.name),
            onPositive = { changeScreen(app) }
        )
    }

    private fun changeScreen(app: SlugApp) {
        BuzzScreen.getInstance().userProfile?.let {
            it.setCustomTarget1(app.slug)
            if (it.gender.isEmpty() || it.birthYear == 0) {
                gotoSetupProfile()
            } else {
                gotoMainScreen()
            }
        }
    }

    private fun gotoMainScreen() {
        changeRoot(MainFragment())
    }

    private fun gotoSetupProfile() {
        replace(ProfileFragment())
    }

    companion object {
        const val KEY_DATA = "data"
    }

}