package com.lockscreenperks.screens.onbroading

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.viewpager.widget.ViewPager
import com.lockscreenperks.R
import com.lockscreenperks.screens.home.HomeActivity
import kotlinx.android.synthetic.main.activity_onbroading.*

class OnbroadingActivity : AppCompatActivity() {

    private val contents: IntArray by lazy {
        intArrayOf(
            R.string.slider_tutorial_1,
            R.string.slider_tutorial_2,
            R.string.slider_tutorial_3,
            R.string.slider_tutorial_4
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onbroading)
        vpOnbroading.adapter = OnbroadingAdapter()
        vpOnbroading.currentItem = 0
        vpOnbroading.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
              changePage(position)
            }
        })
        changePage(0)
        tvSkip.setOnClickListener { gotoHome() }
        btnStart.setOnClickListener { gotoHome() }
    }

    private fun gotoHome() {
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }

    private fun changePage(position: Int) {
        tvContent.setText(contents[position])
        indicatorView.setSelected(position)
        btnStart.isVisible = position == 3
        tvSkip.isInvisible = position == 3
    }

}