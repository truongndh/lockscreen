package com.lockscreenperks.screens.onbroading

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.lockscreenperks.R
import kotlinx.android.synthetic.main.item_onbroading.view.*

class OnbroadingAdapter : PagerAdapter() {

    private val images: IntArray by lazy {
        intArrayOf(
            R.drawable.slider_tutorial_1,
            R.drawable.slider_tutorial_2,
            R.drawable.slider_tutorial_3,
            R.drawable.slider_tutorial_4
        )
    }

    override fun getCount(): Int = images.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context).inflate(R.layout.item_onbroading, container, false)
        view.ivIntro.setImageResource(images[position])
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
        if (view is View)
            container.removeView(view)
    }
}