package com.lockscreenperks.screens.home.model

import com.google.gson.annotations.SerializedName

class SportsTeam {
    @SerializedName("slug")
    var slug: String? = null
    @SerializedName("team_name")
    var teamName: String? = null
}
