package com.lockscreenperks.screens.home.model

import com.google.gson.annotations.SerializedName

class AndroidPackage {
    @SerializedName("program_slug")
    var programSlug: String? = null
    @SerializedName("android_package_name")
    var androidPackageName: String? = null
}