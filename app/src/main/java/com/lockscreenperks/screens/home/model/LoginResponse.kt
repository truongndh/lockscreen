package com.lockscreenperks.screens.home.model

import com.google.gson.annotations.SerializedName

class LoginResponse {
    @SerializedName("gender")
    var gender: String? = null
    @SerializedName("player_age")
    var playerAge: String? = ""
    @SerializedName("player_id")
    var playerId: Int = 0
    @SerializedName("sports_teams")
    var sportsTeams: List<SportsTeam>? = null
    @SerializedName("android_packages")
    var androidPackages: List<AndroidPackage>? = null
}
