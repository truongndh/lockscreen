package com.lockscreenperks.screens.home


import android.app.ProgressDialog
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.buzzvil.buzzscreen.sdk.BuzzScreen
import com.buzzvil.buzzscreen.sdk.UserProfile
import com.lockscreenperks.R
import com.lockscreenperks.api.AppApi
import com.lockscreenperks.extensions.enqueue
import com.lockscreenperks.prefs.AppPrefs
import com.lockscreenperks.screens.Const
import com.lockscreenperks.screens.Const.RESPONSE_CODE_NO_PLAYER_ID
import com.lockscreenperks.screens.Const.RESPONSE_CODE_SUCCESS
import com.lockscreenperks.screens.chooseapp.ChooseAppFragment
import com.lockscreenperks.screens.chooseapp.model.SlugApp
import com.lockscreenperks.screens.home.model.AndroidPackage
import com.lockscreenperks.screens.home.model.LoginResponse
import com.lockscreenperks.screens.home.model.SportsTeam
import com.lockscreenperks.screens.main.MainActivity
import com.lockscreenperks.screens.main.MainFragment
import com.lockscreenperks.screens.profile.ProfileFragment
import com.lockscreenperks.utils.DialogUtil
import com.lockscreenperks.utils.Utils
import kotlinx.android.synthetic.main.activity_home.*
import retrofit2.Response

class HomeActivity : AppCompatActivity() {

    private var mProgressDialog: ProgressDialog? = null
    private val api: AppApi by lazy { AppApi() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        btnEnable.setOnClickListener { enableLockScreen() }
        llTerms.setOnClickListener { gotoTermsAndConditions() }
        llPolicy.setOnClickListener { gotoPrivacyPolicy() }
    }

    private fun enableLockScreen() {
        showLoading()
        api.login(AppPrefs.get().gpsAdId, Const.BUZZVIL_API_KEY)
            .enqueue(success = {
                handleLoginResponse(it)
                hideLoading()
            }, failure = {
                showToastError()
                hideLoading()
            })
    }

    private fun handleLoginResponse(response: Response<LoginResponse>) {
        when (response.code()) {
            RESPONSE_CODE_SUCCESS -> response.body()?.let { handleResponseWithPlayerId(it) }
            RESPONSE_CODE_NO_PLAYER_ID -> handleResponseWithNoPlayerId()
            else -> showToastError()
        }
    }

    private fun handleResponseWithNoPlayerId() {
        DialogUtil.showConfirmDialog(context = this,
            message = getString(R.string.msg_louder_rewards),
            onPositive = { gotoRewardsPage() }
        )
    }

    private fun handleResponseWithPlayerId(response: LoginResponse) {

        BuzzScreen.getInstance().userProfile?.let {
            it.gender = when (response.gender) {
                Const.MALE -> UserProfile.USER_GENDER_MALE
                Const.FEMALE -> UserProfile.USER_GENDER_FEMALE
                else -> ""
            }
            if (!response.playerAge.isNullOrBlank()) {
                it.birthYear = Utils.getCurrentYear() - (response.playerAge?.toInt() ?: 0)
            }
        }
        when {
            (response.sportsTeams?.size ?: 0) > 1 -> response.sportsTeams?.let { openChooseAppSport(it) }
            (response.androidPackages?.size ?: 0) > 1 -> response.androidPackages?.let { openGenericApp(it) }
            (response.sportsTeams?.size ?: 0) == 1 -> response.sportsTeams?.get(0)?.slug?.let { openConfigSlug(it) }
            (response.androidPackages?.size
                ?: 0) == 1 -> response.androidPackages?.get(0)?.programSlug?.let { openConfigSlug(it) }
        }
    }

    private fun openChooseAppSport(sportsTeamList: List<SportsTeam>) {
        val data = Bundle()
        val appList = ArrayList(sportsTeamList.map { SlugApp(it.teamName, it.slug) })
        data.putParcelableArrayList(ChooseAppFragment.KEY_DATA, appList)
        MainActivity.openWithRoot(this, ChooseAppFragment::class.java, data)
        finish()
    }

    private fun openGenericApp(androidPackageList: List<AndroidPackage>) {
        val data = Bundle()
        val appList = ArrayList(androidPackageList.map {
            SlugApp(it.androidPackageName, it.programSlug) })
        data.putParcelableArrayList(ChooseAppFragment.KEY_DATA, appList)
        MainActivity.openWithRoot(this, ChooseAppFragment::class.java, data)
        finish()
    }

    private fun openConfigSlug(slug: String) {
        BuzzScreen.getInstance().userProfile?.let {
            it.setCustomTarget1(slug)
            if (it.gender.isEmpty() || it.birthYear == 0) {
                gotoSetupProfile()
            } else {
                gotoMainScreen()
            }
        }
    }

    private fun gotoMainScreen() {
        MainActivity.openWithRoot(this, MainFragment::class.java)
        finish()
    }

    private fun gotoSetupProfile() {
        MainActivity.openWithRoot(this, ProfileFragment::class.java)
        finish()
    }

    private fun showToastError() {
        Toast.makeText(this, getString(R.string.msg_login_error), Toast.LENGTH_LONG).show()
    }

    private fun gotoTermsAndConditions() {
        Utils.openBrowser(this, Const.URL_TERMS_CONDITIONS)
    }

    private fun gotoPrivacyPolicy() {
        Utils.openBrowser(this, Const.URL_PRIVACY_POLICY)
    }

    private fun gotoRewardsPage() {
        Utils.openBrowser(this, Const.URL_LOUDER_REWARDS_PAGE)
    }

    private fun showLoading() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog(this)
            mProgressDialog?.setCancelable(false)
            mProgressDialog?.setMessage(getString(R.string.waiting))
        }

        mProgressDialog?.show()

    }

    private fun hideLoading() {
        if (mProgressDialog?.isShowing == true) {
            mProgressDialog?.dismiss()
        }
    }
}