package com.lockscreenperks.screens.splash

import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.doOnEnd
import com.buzzvil.buzzscreen.sdk.BuzzScreen
import com.lockscreenperks.R
import com.lockscreenperks.prefs.AppPrefs
import com.lockscreenperks.screens.home.HomeActivity
import com.lockscreenperks.screens.main.MainActivity
import com.lockscreenperks.screens.main.MainFragment
import com.lockscreenperks.screens.onbroading.OnbroadingActivity
import com.lockscreenperks.utils.GetAdvertAsync
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val animator = ObjectAnimator.ofInt(progressBar, "progress", 0, 100)
            .setDuration(2000)
        animator.doOnEnd { gotoScreen() }
        animator.start()

        GetAdvertAsync{}.execute()
    }

    private fun gotoScreen() {
        if (!AppPrefs.get().isIntroShowed) {
            startActivity(Intent(this, OnbroadingActivity::class.java))
            AppPrefs.get().isIntroShowed = true
        } else {
            val userProfile = BuzzScreen.getInstance().userProfile
            if (userProfile.userId.isNotBlank() && userProfile.gender.isNotBlank() && userProfile.birthYear > 0) {
                MainActivity.openWithRoot(this, MainFragment::class.java)
            } else {
                startActivity(Intent(this, HomeActivity::class.java))
            }
        }
        finish()
    }
}