package com.lockscreenperks.screens

object Const {

    const val BUZZVIL_API_KEY = "44374c433fda786d8e8c87d06b28a723"
    const val IS_USE_GDPR = false

    const val DELAY_SEARCH = 200L

    const val DEEPLINK_HOME = "imnbabostonceltics://home"
    const val DEEPLINK_MYAPP = "imnbabostonceltics://myapps"
    const val DEEPLINK_REWARD = "imnbabostonceltics://prizes"

    const val LOGIN_BASE_URL = "https://api.louderrewards.com"
    const val SUGGEST_BASE_URL = "http://buzzvil_cps.cps.ampfeed.com"
    const val SEARCH_BASE_URL = "http://buzzvil_search.ampfeed.com"

    const val URL_TERMS_CONDITIONS = "http://influencemobile.com/terms"
    const val URL_PRIVACY_POLICY = "http://influencemobile.com/privacy"
    const val URL_LOUDER_REWARDS_PAGE = "http://www.louderrewards.com/?buzzvil=y"

    const val RESPONSE_CODE_SUCCESS = 200
    const val RESPONSE_CODE_NO_PLAYER_ID = 403

    const val PARTNER_SUGGEST_NAME = "buzzvil_cps"
    const val PARTNER_SEARCH_NAME = "buzzvil_search"
    const val SUGGEST_SUB1 = "im_cps"
    const val SUGGEST_VERSION = "1.2"
    const val SEARCH_SUB1 = "im_search"
    const val SEARCH_VERSION = "7"
    const val DEFAULT_MOBILE_TYPE = 2
    const val DEFAULT_NUM_WEB_RESULT = 20

    const val MALE = "male"
    const val FEMALE = "female"

    const val MIN_YEAROLD = 14
    const val MAX_YEAROLD = 64

    const val TYPE_JSON = "json"
    const val TIME_FORMAT = "yyyy-MM-dd HH:mm:ss"
}