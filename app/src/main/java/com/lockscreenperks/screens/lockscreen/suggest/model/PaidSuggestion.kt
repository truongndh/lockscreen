package com.lockscreenperks.screens.lockscreen.suggest.model

import com.google.gson.annotations.SerializedName

class PaidSuggestion {

    @SerializedName("id")
    var id: Int = 0
    @SerializedName("term")
    var term: String? = null
    @SerializedName("click_url")
    var clickUrl: String? = null
    var imageUrl: String? = null
    @SerializedName("impression_url")
    var impressionUrl: String? = null
    @SerializedName("label_required")
    var isLabelRequired: Boolean = false

}
