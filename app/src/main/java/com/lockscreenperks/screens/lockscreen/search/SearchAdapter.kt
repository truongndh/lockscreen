package com.lockscreenperks.screens.lockscreen.search

import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.lockscreenperks.R
import com.lockscreenperks.screens.lockscreen.search.model.SearchDisplay
import com.lockscreenperks.screens.lockscreen.search.viewholder.SearchViewHolder
import kotlinx.android.synthetic.main.item_search.view.*

class SearchAdapter(private val onClick: (String) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val searchList: MutableList<SearchDisplay> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SearchViewHolder(parent)
    }

    override fun getItemCount(): Int = searchList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is SearchViewHolder)
            holder.bind(searchList[position])
        holder.itemView.setOnClickListener {
            searchList[position].clickUrl?.let { url -> onClick(url) }
        }
        holder.itemView.divider.isVisible = position != searchList.size - 1
        when (position) {
            0 -> holder.itemView.setBackgroundResource(R.drawable.bg_item_search_top)
            searchList.size - 1 -> holder.itemView.setBackgroundResource(R.drawable.bg_item_search_bottom)
            else -> holder.itemView.setBackgroundResource(R.drawable.bg_item_search_middle)
        }
    }

    fun submitList(data: Collection<SearchDisplay>) {
        searchList.clear()
        searchList.addAll(data)
        notifyDataSetChanged()
    }

    fun clear() {
        searchList.clear()
        notifyDataSetChanged()
    }

}