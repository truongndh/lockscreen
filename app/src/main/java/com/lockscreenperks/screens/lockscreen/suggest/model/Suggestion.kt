package com.lockscreenperks.screens.lockscreen.suggest.model

import com.google.gson.annotations.SerializedName

class Suggestion {

    @SerializedName("original_qt")
    var originalQt: String? = null
    @SerializedName("paid_suggestions")
    var paidSuggestions: List<PaidSuggestion>? = null
    @SerializedName("organic_suggestions")
    var organicSuggestions: List<OrganicSuggestion>? = null

}
