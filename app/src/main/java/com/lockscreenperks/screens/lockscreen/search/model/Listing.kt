package com.lockscreenperks.screens.lockscreen.search.model

import com.google.gson.annotations.SerializedName

class Listing {
    @SerializedName("title")
    var title: String? = null
    @SerializedName("adId")
    var adId: String? = null
    @SerializedName("campaignId")
    var campaignId: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("bidPrice")
    var bidPrice: String? = null
    @SerializedName("clickurl")
    var clickUrl: String? = null
    @SerializedName("displayurl")
    var displayUrl: String? = null
    @SerializedName("impressionurl")
    var impressionUrl: String? = null
}
