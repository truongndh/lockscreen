package com.lockscreenperks.screens.lockscreen.suggest.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class OrganicSuggestion {

    @SerializedName("term")
    var term: String? = null
    @Expose(serialize = false, deserialize = false)
    var text: CharSequence? = null

}