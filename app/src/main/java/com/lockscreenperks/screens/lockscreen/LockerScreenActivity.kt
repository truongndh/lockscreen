package com.lockscreenperks.screens.lockscreen

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.PopupWindow
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.buzzvil.buzzscreen.sdk.BaseLockerActivity
import com.buzzvil.buzzscreen.sdk.Campaign
import com.lockscreenperks.R
import com.lockscreenperks.api.AppApi
import com.lockscreenperks.extensions.enqueue
import com.lockscreenperks.screens.Const
import com.lockscreenperks.screens.lockscreen.search.SearchAdapter
import com.lockscreenperks.screens.lockscreen.search.model.Search
import com.lockscreenperks.screens.lockscreen.search.model.SearchDisplay
import com.lockscreenperks.screens.lockscreen.suggest.SuggestAdapter
import com.lockscreenperks.screens.lockscreen.suggest.model.Suggestion
import com.lockscreenperks.utils.KeyboardUtil
import com.lockscreenperks.utils.Utils
import kotlinx.android.synthetic.main.activity_locker.*
import kotlinx.android.synthetic.main.layout_popup_menu.view.*
import kotlinx.android.synthetic.main.layout_search.*
import java.text.DateFormatSymbols
import java.util.*
import kotlin.collections.ArrayList

class LockerScreenActivity : BaseLockerActivity() {

    private lateinit var suggestAdapter: SuggestAdapter
    private lateinit var searchAdapter: SearchAdapter
    private val api: AppApi by lazy { AppApi() }
    private var timer: Timer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_locker)
        initSearchView()

        ivLockerSearch.setOnClickListener { showSearchView() }
        ivLockerHome.setOnClickListener { Utils.openDeepLink(this, Const.DEEPLINK_HOME) }
        ivLockerStar.setOnClickListener { Utils.openDeepLink(this, Const.DEEPLINK_MYAPP) }
        ivLockerMenu.setOnClickListener { openMenuPopup() }
        slider.setLeftOnSelectListener { landing() }
        slider.setRightOnSelectListener { unlock() }
        setPageIndicators(ivLockerArrowTop, ivLockerArrowBottom)
    }

    private fun initSearchView() {
        suggestAdapter = SuggestAdapter { querySearch(it) }
        listSuggest.layoutManager = LinearLayoutManager(this)
        listSuggest.adapter = suggestAdapter

        searchAdapter = SearchAdapter { openBrowser(it) }
        listResult.layoutManager = LinearLayoutManager(this)
        listResult.adapter = searchAdapter

        edSearch.addTextChangedListener(searchWatcher)
        edSearch.setOnEditorActionListener(editorActionListener)

        rlSearch.setBackgroundResource(R.drawable.bg_search)
        ivBack.setOnClickListener { hideSearchView() }
    }

    override fun onTimeUpdated(cal: Calendar?) {
        cal?.also {
            val hour = cal.get(Calendar.HOUR)
            val time = getString(R.string.time_format, if (hour == 0) 12 else hour, cal.get(Calendar.MINUTE))
            tvLockerTime.text = time

            // set date
            val symbols = DateFormatSymbols()
            val dayName = symbols.shortWeekdays[cal.get(Calendar.DAY_OF_WEEK)]
            tvLockerDate.text = getString(R.string.date_format, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1)
            tvLockerDay.text = dayName
        }
    }

    override fun onCurrentCampaignUpdated(campaign: Campaign?) {
        val landingPoints = campaign?.landingPoints ?: 0
        val unlockPoints = campaign?.unlockPoints ?: 0

        if (landingPoints > 0) {
            slider.setLeftText(getString(R.string.add_font_format, landingPoints))
        } else {
            slider.setLeftText("")
        }

        if (unlockPoints > 0) {
            slider.setRightText(getString(R.string.add_font_format, unlockPoints))
        } else {
            slider.setRightText("")
        }
    }


    private fun openMenuPopup() {
        val popupContentView = LayoutInflater.from(this).inflate(R.layout.layout_popup_menu, null)
        val popup = PopupWindow(this)
        popup.isOutsideTouchable = true
        popup.isFocusable = true
        popup.contentView = popupContentView
        popup.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        popup.width = ViewGroup.LayoutParams.WRAP_CONTENT
        popup.height = ViewGroup.LayoutParams.WRAP_CONTENT
        popupContentView.tvReport.setOnClickListener {
            popup.dismiss()
            openReward()
        }
        popupContentView.tvReward.setOnClickListener {
            popup.dismiss()
            openReport()
        }

        popup.showAsDropDown(ivLockerMenu)
    }

    private fun showSearchView() {
        suggestAdapter.clear()
        searchLayout.isVisible = true
        rlSearch.setBackgroundResource(R.drawable.bg_search)
        edSearch.requestFocus()
        showSuggestion()
        KeyboardUtil.showKeyboard(this)
    }

    private fun hideSearchView() {
        searchLayout.isVisible = false
        edSearch.setText("")
        edSearch.clearFocus()
        KeyboardUtil.hideKeyboard(this)
    }

    private fun showSuggestion() {
        listSuggest.isVisible = true
        listResult.isVisible = false
    }

    private fun showSearchResult() {
        searchAdapter.clear()
        listSuggest.isVisible = false
        listResult.isVisible = true
        divider.isVisible = false
        rlSearch.setBackgroundResource(R.drawable.bg_search)
        KeyboardUtil.hideKeyboard(this)
    }

    private val searchWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (!s.isNullOrBlank()) {
                timer?.cancel()
                timer = Timer()
                timer?.schedule(object : TimerTask() {
                    override fun run() {
                        runOnUiThread { querySuggestion(s.toString()) }
                    }
                }, Const.DELAY_SEARCH)
            }

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            timer?.cancel()
        }
    }

    private val editorActionListener = TextView.OnEditorActionListener { _, actionId, _ ->
        when (actionId) {
            EditorInfo.IME_ACTION_SEARCH -> {
                showSearchResult()
                querySearch(edSearch.text.toString())
            }
        }
        true
    }

    private fun querySuggestion(text: String) {
        showSuggestion()
        api.getSuggestion(key = text)
            .enqueue(success = { response ->
                response.body()?.let {
                    it.organicSuggestions?.forEach { item -> item.text = Utils.highlight(item.term ?: "", text) }
                    handleSuggest(it)
                }

            }, failure = {
                it.printStackTrace()
            })
    }

    private fun querySearch(text: String) {
        showSearchResult()
        timer?.cancel()
        api.search(text)
            .enqueue(success = { response ->
                response.body()?.let { handleSearchResult(it) }
            }, failure = {
                it.printStackTrace()
            })
    }

    private fun handleSuggest(suggestion: Suggestion) {
        if (listSuggest.isVisible) {
            val suggestList = ArrayList<Any>()

            suggestion.paidSuggestions?.forEach {
                suggestList.add(it)
            }
            suggestion.organicSuggestions?.forEach {
                suggestList.add(it)
            }

            if (suggestList.isEmpty()) {
                rlSearch.setBackgroundResource(R.drawable.bg_search)
                divider.isVisible = false
            } else {
                rlSearch.setBackgroundResource(R.drawable.bg_search_top)
                divider.isVisible = true
            }

            suggestAdapter.submitList(suggestList)
        }
    }

    private fun handleSearchResult(search: Search) {
        if (listResult.isVisible) {
            val searchList = ArrayList<SearchDisplay>()

            search.adlistings?.listing?.let { list ->
                searchList.addAll(list.map { SearchDisplay(it.title, it.description, it.displayUrl, it.clickUrl) })
            }

            search.weblistings?.weblisting?.let { list ->
                searchList.addAll(list.map { SearchDisplay(it.title, it.description, it.displayUrl, it.clickUrl) })
            }

            searchAdapter.submitList(searchList)
        }
    }

    private fun openReward() {
        Utils.openDeepLink(this, Const.DEEPLINK_REWARD)
    }

    private fun openReport() {

    }

    private fun openBrowser(url: String) {
        Utils.openBrowser(this, url)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        hideSearchView()
    }

    override fun onDestroy() {
        super.onDestroy()
        timer?.cancel()
    }

}