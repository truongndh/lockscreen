package com.lockscreenperks.screens.lockscreen.suggest.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lockscreenperks.R
import com.lockscreenperks.screens.lockscreen.suggest.model.PaidSuggestion
import com.lockscreenperks.utils.ImageUtil
import kotlinx.android.synthetic.main.item_paid_suggest.view.*

class PaidViewHolder(parent: ViewGroup) :
    RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_paid_suggest, parent, false)) {

    fun bind(data: PaidSuggestion, onClick: (String) -> Unit) {
        ImageUtil.loadImage(data.imageUrl, itemView.ivSuggest)
        itemView.setOnClickListener { data.term?.let { term -> onClick(term) } }
    }
}