package com.lockscreenperks.screens.lockscreen.search.model

data class SearchDisplay(
    var title: String?,
    var description: String?,
    var displayUrl: String?,
    var clickUrl: String?
)