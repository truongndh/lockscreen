package com.lockscreenperks.screens.lockscreen.search.model

import com.google.gson.annotations.SerializedName

class Search {
    @SerializedName("adlistings")
    var adlistings: Adlistings? = null
    @SerializedName("weblistings")
    var weblistings: Weblistings? = null
    @SerializedName("count")
    var count: String? = null
    @SerializedName("webCount")
    var webCount: String? = null
    @SerializedName("response")
    var response: String? = null
}
