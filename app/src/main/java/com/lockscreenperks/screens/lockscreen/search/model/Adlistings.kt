package com.lockscreenperks.screens.lockscreen.search.model

import com.google.gson.annotations.SerializedName

class Adlistings {
    @SerializedName("listing")
    var listing: List<Listing>? = null
}