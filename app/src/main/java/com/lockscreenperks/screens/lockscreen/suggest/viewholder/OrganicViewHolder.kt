package com.lockscreenperks.screens.lockscreen.suggest.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lockscreenperks.R
import com.lockscreenperks.screens.lockscreen.suggest.model.OrganicSuggestion
import kotlinx.android.synthetic.main.item_organic_suggest.view.*

class OrganicViewHolder(parent: ViewGroup) :
    RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_organic_suggest, parent, false)) {

    fun bind(data: OrganicSuggestion, onClick: (String) -> Unit) {
        itemView.tvSuggest.text = data.text
        itemView.setOnClickListener { data.term?.let { term -> onClick(term) } }
    }
}