package com.lockscreenperks.screens.lockscreen.search.model

import com.google.gson.annotations.SerializedName

class Weblisting {
    @SerializedName("title")
    var title: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("clickurl")
    var clickUrl: String? = null
    @SerializedName("displayurl")
    var displayUrl: String? = null
}