package com.lockscreenperks.screens.lockscreen.suggest

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lockscreenperks.R
import com.lockscreenperks.screens.lockscreen.suggest.model.OrganicSuggestion
import com.lockscreenperks.screens.lockscreen.suggest.model.PaidSuggestion
import com.lockscreenperks.screens.lockscreen.suggest.viewholder.OrganicViewHolder
import com.lockscreenperks.screens.lockscreen.suggest.viewholder.PaidViewHolder

class SuggestAdapter(private val onClick: (String) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_PAID = 0
        private const val TYPE_ORGANIC = 1
    }

    private val suggestList = ArrayList<Any>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_PAID -> PaidViewHolder(parent)
            else -> OrganicViewHolder(parent)
        }
    }

    override fun getItemCount(): Int = suggestList.size

    override fun getItemViewType(position: Int): Int {
        return when (suggestList[position]) {
            is PaidSuggestion -> TYPE_PAID
            else -> TYPE_ORGANIC
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is PaidViewHolder -> holder.bind(suggestList[position] as PaidSuggestion, onClick)
            is OrganicViewHolder -> holder.bind(suggestList[position] as OrganicSuggestion, onClick)
        }
        if (position == suggestList.size - 1) {
            holder.itemView.setBackgroundResource(R.drawable.bg_item_search_bottom)
        } else {
            holder.itemView.setBackgroundResource(R.drawable.bg_item_search_middle)
        }
    }

    fun submitList(suggestList: ArrayList<Any>) {
        this.suggestList.clear()
        this.suggestList.addAll(suggestList)
        notifyDataSetChanged()
    }

    fun clear() {
        this.suggestList.clear()
        notifyDataSetChanged()
    }

}