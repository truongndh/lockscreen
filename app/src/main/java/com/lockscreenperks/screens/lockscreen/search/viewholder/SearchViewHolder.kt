package com.lockscreenperks.screens.lockscreen.search.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lockscreenperks.R
import com.lockscreenperks.screens.lockscreen.search.model.SearchDisplay
import kotlinx.android.synthetic.main.item_search.view.*

class SearchViewHolder(parent: ViewGroup) :
    RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_search, parent, false)) {

    fun bind(data: SearchDisplay) {
        itemView.tvTitle.text = data.title
        itemView.tvDescription.text = data.description
        itemView.tvUrl.text = data.displayUrl
    }
}