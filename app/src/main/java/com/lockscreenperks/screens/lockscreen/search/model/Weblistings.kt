package com.lockscreenperks.screens.lockscreen.search.model

import com.google.gson.annotations.SerializedName

class Weblistings {
    @SerializedName("weblisting")
    var weblisting: List<Weblisting>? = null
    @SerializedName("totalMatches")
    var totalMatches: String? = null
}