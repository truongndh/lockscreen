package com.lockscreenperks.screens.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.buzzvil.buzzscreen.sdk.BuzzScreen
import com.buzzvil.buzzscreen.sdk.UserProfile
import com.lockscreenperks.R
import com.lockscreenperks.extensions.changeRoot
import com.lockscreenperks.screens.Const
import com.lockscreenperks.screens.main.MainFragment
import com.lockscreenperks.utils.DialogUtil
import com.lockscreenperks.utils.Utils
import kotlinx.android.synthetic.main.fragment_profile.*


class ProfileFragment : Fragment() {

    private val yearArray: Array<String> by lazy {
        val currentYear = Utils.getCurrentYear()
        val list = arrayListOf<String>()
        for (i in Const.MIN_YEAROLD..Const.MAX_YEAROLD) {
            list.add("${currentYear - i}")
        }
        list.toTypedArray()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvMale.setOnClickListener { selectMale() }

        tvFemale.setOnClickListener { selectFemale() }
        verifyDone()

        tvYear.setOnClickListener { showDialogDateChooser() }

        btnDone.setOnClickListener { setUserProfile() }

        BuzzScreen.getInstance().userProfile?.let {
            if (it.birthYear > 0) tvYear.text = it.birthYear.toString()

            tvMale.isSelected = it.gender == UserProfile.USER_GENDER_MALE
            tvFemale.isSelected = it.gender == UserProfile.USER_GENDER_FEMALE
        }
    }

    private fun selectFemale() {
        tvMale.isSelected = false
        tvFemale.isSelected = true
        verifyDone()
    }

    private fun selectMale() {
        tvMale.isSelected = true
        tvFemale.isSelected = false
        verifyDone()
    }

    private fun setUserProfile() {
        BuzzScreen.getInstance().userProfile?.let {
            it.birthYear = tvYear.text.toString().toInt()
            it.gender = if (tvFemale.isSelected) UserProfile.USER_GENDER_FEMALE else UserProfile.USER_GENDER_MALE
        }
        changeRoot(MainFragment())
    }

    private fun showDialogDateChooser() {
        DialogUtil.showSingleChoiceDialog(context = context,
            title = getString(R.string.year_of_birth_title),
            items = yearArray,
            onPositive = { bindSelectYear(it) }
        )
    }

    private fun bindSelectYear(selectIndex: Int) {
        tvYear.text = yearArray[selectIndex]
        verifyDone()
    }

    private fun verifyDone() {
        btnDone.isEnabled = tvYear.text.isNotEmpty() && (tvMale.isSelected || tvFemale.isSelected)
    }
}