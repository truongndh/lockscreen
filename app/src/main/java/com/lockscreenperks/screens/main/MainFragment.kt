package com.lockscreenperks.screens.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.buzzvil.buzzcore.utils.TimeUtils
import com.buzzvil.buzzscreen.sdk.BuzzScreen
import com.lockscreenperks.R
import com.lockscreenperks.screens.Const
import com.lockscreenperks.utils.DialogUtil
import com.lockscreenperks.utils.SnackbarUtil
import com.lockscreenperks.utils.Utils
import kotlinx.android.synthetic.main.fragment_home.*


class MainFragment : Fragment() {

    private var lastTimeSmoze:Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvLouder.setOnClickListener { Utils.openDeepLink(context, Const.DEEPLINK_HOME) }
        tvPoint.setOnClickListener { Utils.openDeepLink(context, Const.DEEPLINK_MYAPP) }
        tvReward.setOnClickListener { Utils.openDeepLink(context, Const.DEEPLINK_REWARD) }

        tvTerms.setOnClickListener { Utils.openBrowser(context, Const.URL_TERMS_CONDITIONS) }
        tvPrivacy.setOnClickListener { Utils.openBrowser(context, Const.URL_PRIVACY_POLICY) }

        switchActive.setOnClickListener { switchLockScreen() }

        if (BuzzScreen.getInstance().userProfile.userId.isNullOrBlank()) {
            BuzzScreen.getInstance().userProfile.userId = Utils.genRandomUUID()
            activeLockScreen()
        }

        if (BuzzScreen.getInstance().isActivated) {
            val snooze = 0
            if (BuzzScreen.getInstance().isSnoozed) {
                snooze - BuzzScreen.getInstance().snoozeTo - TimeUtils.getCurrentTimestamp()
            }
            BuzzScreen.getInstance().activate()
            BuzzScreen.getInstance().snooze(snooze)
        }

    }

    override fun onStart() {
        super.onStart()
        updateSwitchButton()
    }

    private fun activeLockScreen() {
        if (Const.IS_USE_GDPR) {
            BuzzScreen.getInstance().activateIfConsent(activity, object : BuzzScreen.OnActivateListener {
                override fun onActivated() {
                    updateSwitchButton()
                }

                override fun onCancelledByUser() {

                }

                override fun onNetworkError() {
                }
            })
        } else {
            BuzzScreen.getInstance().activate()
            updateSwitchButton()
        }

    }

    private fun deactiveLockScreen(index: Int) {
        val timeArray = resources.getIntArray(R.array.list_time_off_lockscreen_value)
        switchActive.isChecked = false
        if (index == timeArray.size) {
            BuzzScreen.getInstance().deactivate()
            lastTimeSmoze = 0
            showSnackbar(getString(R.string.lockscreen_deactive))
        } else {
            val time = timeArray[index]
            BuzzScreen.getInstance().snooze(time)
            lastTimeSmoze = time
            showSnackbar(getString(R.string.lockscreen_deactive_snooze, Utils.getNextTime(time * 1000)))
        }

    }

    private fun showDialogChooserTimeDeactive() {
        DialogUtil.showSingleChoiceDialog(context = context,
            title = getString(R.string.turn_off_lockscreen),
            items = resources.getStringArray(R.array.list_time_off_lockscreen),
            onNegative = {
                updateSwitchButton()
            },
            onPositive = {
                deactiveLockScreen(it)
                updateSwitchButton()
            })
    }

    private fun showSnackbar(msg: String) {
        SnackbarUtil.showSnackbar(
            rootView = view,
            msg = msg,
            action = getString(R.string.undo),
            onAction = {
                undo()
                updateSwitchButton()
            })
    }

    private fun switchLockScreen() {
        if (isActivatedNow()) {
            showDialogChooserTimeDeactive()
        } else {
            activeLockScreen()
            showSnackbar(getString(R.string.lockscreen_active))
        }
    }

    private fun updateSwitchButton() {
        switchActive.isChecked = isActivatedNow()
    }


    private fun undo() {
        if (!BuzzScreen.getInstance().isActivated || BuzzScreen.getInstance().isSnoozed) {
            activeLockScreen()
        } else if (BuzzScreen.getInstance().isActivated) {
            if (lastTimeSmoze > 0)
                BuzzScreen.getInstance().snooze(lastTimeSmoze)
            else BuzzScreen.getInstance().deactivate()
        }
    }

    private fun isActivatedNow(): Boolean {
        return BuzzScreen.getInstance().isActivated && !BuzzScreen.getInstance().isSnoozed
    }
}