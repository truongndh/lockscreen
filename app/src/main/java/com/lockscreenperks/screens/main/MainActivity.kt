package com.lockscreenperks.screens.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.lockscreenperks.R
import com.lockscreenperks.extensions.StyleAnimation
import com.lockscreenperks.extensions.replace

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        intent.getStringExtra(KEY_NAME)?.let {
            try {
                val fragment = Class.forName(it).newInstance()
                if (fragment is Fragment) {
                    fragment.arguments = intent.extras
                    replace(fragment, isAddToStack = false, animation = StyleAnimation.SLIDE_NONE)
                }
            } catch (e: InstantiationException) {
            } catch (e: IllegalAccessException) {
            } catch (e: ClassNotFoundException) {
            }

        }
    }

    companion object {
        private const val KEY_NAME = "fragment.name"
        fun openWithRoot(context: Context, fragment: Class<*>, data: Bundle? = null) {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(KEY_NAME, fragment.canonicalName)
            data?.let { intent.putExtras(data) }
            context.startActivity(intent)
        }
    }

}
