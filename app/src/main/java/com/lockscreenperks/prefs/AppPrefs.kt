package com.lockscreenperks.prefs

import android.content.SharedPreferences
import com.lockscreenperks.App
import com.lockscreenperks.extensions.PreferenceHelper
import com.lockscreenperks.extensions.get
import com.lockscreenperks.extensions.set

class AppPrefs private constructor() {

    private val prefs: SharedPreferences by lazy { PreferenceHelper.newPrefs(App.get(), "app_prefs") }

    var isIntroShowed: Boolean
        get() = prefs[KEY_INTRO, false] ?: false
        set(value) {
            prefs[KEY_INTRO] = value
        }

    var gpsAdId: String
        get() = prefs[KEY_AD_ID] ?: ""
        set(value) {
            prefs[KEY_AD_ID] = value
        }

    companion object {
        private const val KEY_INTRO = "is.show.intro"
        private const val KEY_AD_ID = "is.ad.id"

        private val appPrefs: AppPrefs by lazy { AppPrefs() }

        fun get() = appPrefs
    }

}