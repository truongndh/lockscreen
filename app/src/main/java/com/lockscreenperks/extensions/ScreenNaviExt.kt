package com.lockscreenperks.extensions

import android.app.Activity
import android.content.Intent
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.lockscreenperks.R
import kotlin.reflect.KClass

enum class StyleAnimation {
    SLIDE_FROM_RIGHT, SLIDE_FROM_LEFT, SLIDE_FROM_BOTTOM, SLIDE_NONE
}

fun <T : Activity> KClass<T>.start(activity: Activity, finish: Boolean = false) {
    activity.startActivity(Intent(activity, this.java))
    if (finish) {
        activity.finish()
    }
}

fun <T : Activity> KClass<T>.startClearTop(activity: Activity) {
    val intent = Intent(activity, this.java)
    activity.startActivity(intent)
    activity.finishAffinity()

}

fun FragmentActivity.replace(
    fragment: Fragment,
    holder: Int = R.id.container,
    isAddToStack: Boolean = true,
    animation: StyleAnimation = StyleAnimation.SLIDE_FROM_RIGHT
) {
    val transaction = supportFragmentManager.beginTransaction()
    transaction.run {
        setAnimation(this, animation)
        replace(holder, fragment)
        if (isAddToStack)
            addToBackStack(null)
        commit()
    }
}

fun FragmentActivity.add(
    fragment: Fragment,
    holder: Int = R.id.container,
    isAddToStack: Boolean = true,
    animation: StyleAnimation = StyleAnimation.SLIDE_FROM_RIGHT
) {
    val transaction = supportFragmentManager.beginTransaction()
    transaction.run {
        setAnimation(this, animation)
        add(holder, fragment)
        if (isAddToStack)
            addToBackStack(null)
        commit()
    }
}

fun Fragment.replace(
    fragment: Fragment,
    holder: Int = R.id.container,
    isAddToStack: Boolean = true,
    animation: StyleAnimation = StyleAnimation.SLIDE_FROM_RIGHT
) {
    val transaction = fragmentManager?.beginTransaction()
    transaction?.run {
        setAnimation(this, animation)
        replace(holder, fragment)
        if (isAddToStack)
            addToBackStack(null)
        commit()
    }
}

fun Fragment.add(
    fragment: Fragment,
    holder: Int = R.id.container,
    isAddToStack: Boolean = true,
    animation: StyleAnimation = StyleAnimation.SLIDE_FROM_RIGHT
) {
    val transaction = fragmentManager?.beginTransaction()
    transaction?.run {
        setAnimation(this, animation)
        add(holder, fragment)
        if (isAddToStack)
            addToBackStack(null)

        commit()
    }
}

fun Fragment.changeRoot(fragment: Fragment, holder: Int = R.id.container) {
    fragmentManager?.let { fragmentManager ->
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(holder, fragment)
        if (fragmentManager.backStackEntryCount > 0) {
            val first = fragmentManager.getBackStackEntryAt(0)
            first.let {
                fragmentManager.popBackStack(it.id, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }
        }
        transaction.commit()
    }
}

fun Fragment.backToRootFragment() {
    fragmentManager?.let { fragmentManager ->
        if (fragmentManager.backStackEntryCount > 0) {
            val first = fragmentManager.getBackStackEntryAt(0)
            first.let {
                fragmentManager.popBackStack(it.id, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }
        }
    }
}

private fun setAnimation(transaction: FragmentTransaction, styleAnimation: StyleAnimation) {
    when (styleAnimation) {
        StyleAnimation.SLIDE_FROM_RIGHT -> transaction.setCustomAnimations(
            R.anim.slide_in_from_right,
            R.anim.slide_out_to_left,
            R.anim.slide_in_from_left,
            R.anim.slide_out_to_right
        )
        StyleAnimation.SLIDE_FROM_LEFT -> transaction.setCustomAnimations(
            R.anim.slide_in_from_left,
            R.anim.stack_pop,
            R.anim.stack_push,
            R.anim.slide_out_to_left
        )
        StyleAnimation.SLIDE_FROM_BOTTOM -> transaction.setCustomAnimations(
            R.anim.slide_in_bottom,
            R.anim.stack_push,
            R.anim.stack_pop,
            R.anim.slide_out_bottom
        )
        else -> {
        }
    }
}

fun Fragment.goBack(): Boolean {
    return if (activity != null) {
        activity?.onBackPressed()
        true
    } else false


}

fun FragmentActivity.activePage(holder: Int = R.id.container): Fragment? {
    return supportFragmentManager.findFragmentById(holder)
}

fun Fragment.activePage(holder: Int = R.id.container): Fragment? {
    return fragmentManager?.findFragmentById(holder)
}