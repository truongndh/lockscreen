package com.lockscreenperks

import androidx.multidex.MultiDexApplication
import com.buzzvil.buzzscreen.sdk.BuzzScreen
import com.lockscreenperks.screens.Const
import com.lockscreenperks.screens.lockscreen.LockerScreenActivity


class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        myApplication = this
        BuzzScreen.init(
            getString(R.string.buzz_screen_unit_id),
            this,
            LockerScreenActivity::class.java,
            R.drawable.image_on_fail,
            if (Const.IS_USE_GDPR) BuzzScreen.PrivacyPolicy.GDPR else BuzzScreen.PrivacyPolicy.NONE
        )

        BuzzScreen.getInstance().lockerServiceNotificationConfig.apply {
            title = getString(R.string.app_name)
            text = getString(R.string.msg_notification)
            largeIconResourceId = R.drawable.ic_influence_logo
            smallIconResourceId = R.drawable.ic_influence_logo
        }
    }

    companion object {
        private lateinit var myApplication: App

        fun get() = myApplication
    }
}